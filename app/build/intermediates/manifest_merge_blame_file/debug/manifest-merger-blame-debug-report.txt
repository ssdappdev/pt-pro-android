1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.emersonclimate.PTPro"
4    android:installLocation="auto"
5    android:versionName="3.1" >
6
7    <uses-sdk
7-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:7:5-43
8        android:minSdkVersion="24"
8-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:7:15-40
9        android:targetSdkVersion="28" />
9-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:7:5-43
10
11    <uses-permission android:name="android.permission.INTERNET" />
11-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:9:5-67
11-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:9:22-64
12    <uses-permission android:name="android.permission.WAKE_LOCK" />
12-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:10:5-68
12-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:10:22-65
13    <uses-permission android:name="android.permission.CALL_PHONE" />
13-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:11:5-69
13-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:11:22-66
14
15    <application
15-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:13:5-39:19
16        android:appComponentFactory="android.support.v4.app.CoreComponentFactory"
16-->[com.android.support:support-compat:28.0.0] /Users/mobileapps/.gradle/caches/transforms-2/files-2.1/00567bd0de14b33d1bf8a440dc72bff8/support-compat-28.0.0/AndroidManifest.xml:22:18-91
17        android:debuggable="true"
18        android:extractNativeLibs="false"
19        android:icon="@drawable/ic_launcher1"
19-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:14:9-46
20        android:label="@string/app_name" >
20-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:15:9-41
21        <activity
21-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:16:9-25:20
22            android:name="com.emersonclimate.PTPro.Main"
22-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:17:13-33
23            android:screenOrientation="portrait"
23-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:18:13-49
24            android:windowSoftInputMode="stateHidden" >
24-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:19:13-54
25            <intent-filter>
25-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:20:13-24:29
26                <action android:name="android.intent.action.MAIN" />
26-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:21:17-69
26-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:21:25-66
27
28                <category android:name="android.intent.category.LAUNCHER" />
28-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:23:17-77
28-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:23:27-74
29            </intent-filter>
30        </activity>
31        <activity
31-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:26:9-29:57
32            android:name="com.emersonclimate.PTPro.Information"
32-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:27:13-40
33            android:screenOrientation="portrait"
33-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:28:13-49
34            android:windowSoftInputMode="stateHidden" />
34-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:29:13-54
35
36        <meta-data
36-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:30:9-32:92
37            android:name="LOCALYTICS_APP_KEY"
37-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:31:13-46
38            android:value="a91f9b47d3d7040c58168f8-7e26a164-29ff-11e5-ddd9-00f078254409" />
38-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:32:13-89
39        <meta-data
39-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:33:9-35:72
40            android:name="com.crashlytics.ApiKey"
40-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:34:13-50
41            android:value="e967cdac2aaba0bfea3d71d53665d03ec3b5dc2d" />
41-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:35:13-69
42        <meta-data
42-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:36:9-38:72
43            android:name="io.fabric.ApiKey"
43-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:37:13-44
44            android:value="6cf791bbae89c94278c697fb6b610564c1be9dfa" />
44-->/Users/mobileapps/pt-pro-android/app/src/main/AndroidManifest.xml:38:13-69
45    </application>
46
47</manifest>
