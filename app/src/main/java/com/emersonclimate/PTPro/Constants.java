package com.emersonclimate.PTPro;

public class Constants {
	//temperature constants
	public static final float MIN_TEMP_IN_FAHRENHEIT = -50.0f;
	public static final float MAX_TEMP_IN_FAHRENHEIT = 150.0f;
	public static final float MIN_TEMP_IN_CELSIUS = -45.6f;
	public static final float MAX_TEMP_IN_CELSIUS = 65.6f;
	
	//out of bounds values
	public static final float PRESSURE_OUT_OF_BOUNDS = 1000.0f;
	public static final int TEMP_OUT_OF_BOUNDS = 1000;	
	public static final float TEMP_OUT_OF_BOUNDS_FLOAT = 1000.0f;
	
	//Shared Preferences
	public static String SHARED_PREFERENCES_DISCLAIMER = "Disclaimer";
	public static String SHARED_PREFERENCES_BOOLEAN_VALUE = "BooleanValue";
	public static String PTPRO_LASTREFRIGERANTINDEX = "PTPRO_LASTREFRIGERANTINDEX";
}