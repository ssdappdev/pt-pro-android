/*
 * Created by Numlock Solutions, Co on 18 August 2011
 * Copyright 2011 Emerson Climate Technologies. All rights reserved.
 * 
 */

package com.emersonclimate.PTPro;

import android.view.View;

public class Refrigerant {
	
	//total number of refrigerant
	public static final int REFRIGERANTS_COUNT = 24;
	//index order is based on order in array - please do not change unless order is also changed in arrays
	public static final int R410A = 0;
	public static final int R12 = 1;
	public static final int R22 = 2;
	public static final int R134A = 3;
	public static final int R404A = 4;
	public static final int R407AV = 5;
	public static final int R407AL = 6;
	public static final int R407CV = 7;
	public static final int R407CL = 8;
	public static final int R408A = 9;
	public static final int R409A = 10;
	public static final int R401A = 11;
	public static final int R502 = 12;
	public static final int R507A = 13;
	public static final int R600 = 14;
	public static final int R290 = 15;
	public static final int RCO2 = 16;
	public static final int R407F = 17;
	public static final int R402A = 18;
	public static final int R717 = 19;
	public static final int R448AL = 20;
	public static final int R448AV = 21;
	public static final int R450AL = 22;
	public static final int R450AV = 23;
	
	//refrigerant arrays
	private static final String[] REFRIGERANT_NAMES = new String[]{"R-410A","R-12", "R-22", "R-134a", "R-404A", "R-407A Vapor", "R-407A Liquid", "R-407C Vapor", "R-407C Liquid", "R-408A", "R-409A", "R-401A", "R-502", "R-507A", "R-600", "R-290", "R-CO2", "R-407F", "R-402A", "R-717","R-448A Liquid","R-448A Vapor","R-450A Liquid","R-450A Vapor"};
	private static final int[] REFRIGERANT_IMAGES = new int[]{R.drawable.refrigerant_r_410a, R.drawable.refrigerant_r_12, R.drawable.refrigerant_r_22, R.drawable.refrigerant_r_134a, R.drawable.refrigerant_r_404a, R.drawable.refrigerant_r_407a, R.drawable.refrigerant_r_407a, R.drawable.refrigerant_r_407c, R.drawable.refrigerant_r_407c, R.drawable.refrigerant_r_408a, R.drawable.refrigerant_r_409a, R.drawable.refrigerant_r_401a, R.drawable.refrigerant_r_502, R.drawable.refrigerant_r_507a, R.drawable.refrigerant_r_600, R.drawable.refrigerant_r_290, R.drawable.refrigerant_r_744, R.drawable.refrigerant_r_407f, R.drawable.refrigerant_r_402a, R.drawable.refrigerant_r_717,
		R.drawable.refrigerant_r_448a,R.drawable.refrigerant_r_448a,R.drawable.refrigerant_r_450a,R.drawable.refrigerant_r_450a};
	private static final String[] REFRIGERANT_TABLENAMES = new String[]{"R410A", "R12", "R22", "R134A", "R404AHP62", "R407AVAPOR", "R407ALIQUID", "R407CVAPOR", "R407CLIQUID", "R408A", "R409A", "R401A", "R502", "R507AAZ50", "R600", "R290", "RCO2", "R407F", "R402A", "R717","R448ALIQUID","R448AVAPOR","R450ALIQUID","R450AVAPOR"};
	
	//attributes
	private String name;
	private int image;
	private String tableName;
	private View view;
	private float maxTempC;
	private float maxTempF;
	private float minTempC;
	private float minTempF;
	
	public Refrigerant(View view, int index){
		this.name = REFRIGERANT_NAMES[index];
		this.image = REFRIGERANT_IMAGES[index];
		this.tableName = REFRIGERANT_TABLENAMES[index];
		this.view = view;
	}
	
	public String getName(){
		return this.name;
	}
	
	public int getImage(){
		return this.image;
	}
	
	public String getTableName(){
		return this.tableName;
	}
	
	public View getView(){
		return this.view;
	}

	public void setView(View v){
		this.view = v;
	}

	public float getMaxTempC() {
		return maxTempC;
	}

	public void setMaxTempC(float maxTempC) {
		this.maxTempC = maxTempC;
	}

	public float getMaxTempF() {
		return maxTempF;
	}

	public void setMaxTempF(float maxTempF) {
		this.maxTempF = maxTempF;
	}

	public float getMinTempC() {
		return minTempC;
	}

	public void setMinTempC(float minTempC) {
		this.minTempC = minTempC;
	}

	public float getMinTempF() {
		return minTempF;
	}

	public void setMinTempF(float minTempF) {
		this.minTempF = minTempF;
	}
	
	
}
