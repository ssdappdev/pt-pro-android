/*
 * Created by Numlock Solutions, Co on 08 August 2011
 * Copyright 2011 Emerson Climate Technologies. All rights reserved.
 * 
 */

package com.emersonclimate.PTPro;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;


public class Main extends FragmentActivity{
	
	//static variables
	public static final String TAG = "EMERSON PTPro";
	
	//declare variables
	private EmersonPtDBadapter dbHelper;
	private Display display;
	
	private boolean isSeaLevel, isVacuum;
	private int width, currentRefrigerantIndex, position, scroll;
	
	private Button buttonScrollLeft, buttonScrollRight;
	private ImageButton ibtnInfo;
	private ImageView ivLogo;
	//private TextView tvPressureUnit, tvSealevel, tvRefrigerantbar;
	private TextView tvPressureUnit, tvSealevel;
	private EditText etPressure, etTemperature, etbulbtemperature;
	private TextView etsuperheat, tvCurrentPage,txtSeaLevel;
	//private LinearLayout llIconsrow, hsvParentLinearLayout, llSegmentedRow, llEditTextRow;
	private LinearLayout llIconsrow, llSegmentedRow, llEditTextRow,btnSeaLevel;
	private CustomViewPager hsScroll;
	private SegmentedControl scPsig, scBarg, scFahrenheit, scCelsius;
	private LayoutInflater vi;
	private Refrigerant[] refrigerants = new Refrigerant[Refrigerant.REFRIGERANTS_COUNT];
	private CountDownTimer cdt;
	
	private SharedPreferences sharedPreferences;
	private SharedPreferences.Editor sharedPreferencesEditor;
	
	private boolean pressureChanged = false;
	private boolean temperatureChanged = false;
	private boolean isFromPressure = true;
	
	// Added to detect if swiping left or right
	private boolean isGoingRight = true;
	
	//Localytics
	//private final static String LOCALYTICS_APP_KEY = "cd97133d3dc2c0e42410c51-bc405988-6322-11e4-a6bb-009c5fda0a25";
	private final static String LOCALYTICS_APP_KEY = "bb3dadb5e7611392285f804-7d13f176-58a4-11e6-18fc-00cef1388a40";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main);
        
        sharedPreferences = getSharedPreferences(Constants.SHARED_PREFERENCES_DISCLAIMER, 0);
	    sharedPreferencesEditor = sharedPreferences.edit();
	    
	    if (sharedPreferences.getBoolean(Constants.SHARED_PREFERENCES_BOOLEAN_VALUE, false) == false)
	    	showDialog();
        
        //create Database
        dbHelper = new EmersonPtDBadapter(this);
        dbHelper.createDB();
        
        initLayout();
        
        setLayoutPadding();
        
        // set default localization to avoid errors!
        String lang = Locale.getDefault().getLanguage();
        
        if (!lang.equals("zh"))
        {
        	Locale locale = new Locale("en");
        	Locale.setDefault(locale);
        }
        
        
        setDefaultValues();
		
        // set the pager adapter
        MyCustomPagerAdapter adapter = new MyCustomPagerAdapter();
        
        this.hsScroll.setAdapter(adapter);
        hsScroll.setCurrentItem(0);
        currentRefrigerantIndex = 0;
		setCurrentRefrigerantValues();
        btnSeaLevel.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		
        		//get current refrigerant table name
        		String tableName = getCurrentRefrigerantTable();
        		
        		setTextViewValues();
        		//for pressure edit text
            		float oldPressure = parseReplace(etPressure.getText().toString());//Float.parseFloat(etPressure.getText().toString());
	        		
            		if (isVacuum && scPsig.isChecked())
	        			oldPressure = oldPressure * -1.0f;
	        		
	        		float temperature = getTemperatureGivenPressure(oldPressure,  tableName);
	        		
	        		if (temperature == Constants.TEMP_OUT_OF_BOUNDS)
	        			displayErrorMessage(R.string.error_message_temperature);
	        		else{
	        			
	        			//for button image and sea level text view
	        			//must set SeaLevel value here, after getting oldPressure (since oldPressure should be based on previous sea level value) and before getting newPressure
	            		if (isSeaLevel){
	            			isSeaLevel = false;
	            			txtSeaLevel.setText(getString(R.string.main_highaltitude));
	            			tvSealevel.setText(R.string.abovesea_level);
	            			
	            		} else {
	            			isSeaLevel = true;
	            			txtSeaLevel.setText(getString(R.string.main_sealevel));
	            			tvSealevel.setText(R.string.sea_level);
	            		}
	            		
	            		//set the pressure in psig, given the temperature in farenheit
	        			float newPressure = getPressureGivenTemperature(temperature, refrigerants[currentRefrigerantIndex]);
	        			
	        			newPressure = checkPressureIsWithinRange(newPressure);
	        			
	        			//for pressure unit label
	            		setPressureUnitLabelGivenPressure(newPressure, tableName);
	            		
	            		if (isVacuum && scPsig.isChecked())
	            			newPressure = Math.abs(newPressure);
	            		
	        			etPressure.setText(String.format("%.02f", newPressure));           		
	            		
	            		//for min max values
	            		//setMinMaxPressureLabel(refrigerants[currentRefrigerantIndex].getView(), tableName, refrigerants[currentRefrigerantIndex]);	 
	                    Refrigerant r=  setMinMaxPressureLabel(refrigerants[currentRefrigerantIndex].getView(), tableName, refrigerants[currentRefrigerantIndex]);
	                    refrigerants[currentRefrigerantIndex] = r;
	                    
	                    reloadViewPager();
	                    computeSuperHeat();
	        		}
	        	}
        	
        });
        
        //launch information view
        ibtnInfo.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		Intent info = new Intent(Main.this, Information.class);
        		startActivity(info);
        	}
        });
        etPressure.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		etPressure.setText("");
        	}
        });
        
        etTemperature.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		etTemperature.setText("");
        	}
        });
        
        etbulbtemperature.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		etbulbtemperature.setText("");
        	}
        });
        //if user enters a value in pressure text field
        etPressure.setOnEditorActionListener(new TextView.OnEditorActionListener() {
        	
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			       if(actionId==EditorInfo.IME_ACTION_DONE){
		            	pressureChanged = true;
		            	isFromPressure = true;
		            	
		                	float temperature = Constants.MIN_TEMP_IN_FAHRENHEIT;
		                	float pressure = 0.0f;
		                	
		                	//get current refrigerant
		            		String tableName = getCurrentRefrigerantTable();
		            		
		            		//for temperature edit text
		            		try{
		            			pressure = parseReplace(etPressure.getText().toString());//Float.parseFloat(etPressure.getText().toString());            			
		            		}catch(NumberFormatException e){
		            			e.printStackTrace();
		            		}
		            		
		            		pressure = checkPressureIsWithinRange(pressure);
		        			
		        			temperature = getTemperatureGivenPressure(pressure, tableName);
		        			
		        			temperature = checkTemperatureIsWithinRange(temperature);
		        			
		        			etTemperature.setText(String.format("%.01f", temperature));
		        			
		        			//for pressure unit label
		            		setPressureUnitLabelGivenPressure(pressure, tableName);

		        			if (scPsig.isChecked())
		        				pressure = Math.abs(pressure);
		        			
		        			etPressure.setText(String.format("%.02f", pressure));
		            		
		            		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
		      		  		inputManager.hideSoftInputFromWindow(etPressure.getWindowToken(), 0);  
		      		  		computeSuperHeat(); 
			        }
				return false;
			}
		});

        etPressure.setOnKeyListener(new OnKeyListener(){
            public boolean onKey(View v, int keyCode, KeyEvent event){         
            	pressureChanged = true;
            	isFromPressure = true;
            	
                if (event.getAction()!=KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER){
                	float temperature = Constants.MIN_TEMP_IN_FAHRENHEIT;
                	float pressure = 0.0f;
                	
                	//get current refrigerant
            		String tableName = getCurrentRefrigerantTable();
            		
            		//for temperature edit text
            		try{
            			pressure = parseReplace(etPressure.getText().toString());//Float.parseFloat(etPressure.getText().toString());            			
            		}catch(NumberFormatException e){
            			e.printStackTrace();
            		}
            		
            		pressure = checkPressureIsWithinRange(pressure);
        			
        			temperature = getTemperatureGivenPressure(pressure, tableName);
        			
        			temperature = checkTemperatureIsWithinRange(temperature);
        			
        			etTemperature.setText(String.format("%.01f", temperature));
        			
        			//for pressure unit label
            		setPressureUnitLabelGivenPressure(pressure, tableName);

        			if (scPsig.isChecked())
        				pressure = Math.abs(pressure);
        			
        			etPressure.setText(String.format("%.02f", pressure));
            		
            		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
      		  		inputManager.hideSoftInputFromWindow(etTemperature.getWindowToken(), 0);  
      		  		computeSuperHeat();
                }                
                return false;
            }
        });
        etTemperature.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				//
            	temperatureChanged = true;
            	isFromPressure = false;
            	//
			       if(actionId==EditorInfo.IME_ACTION_DONE){
                	float pressure = 0.0f;
                	float temperature = 0.0f;
                	
                	//get current refrigerant table name
            		String tableName = getCurrentRefrigerantTable();
            		
            		try{
            			temperature = parseReplace(etTemperature.getText().toString());//Float.parseFloat(etTemperature.getText().toString());
            		}catch(NumberFormatException e){
            			e.printStackTrace();
            		}
            		
                	temperature = checkTemperatureIsWithinRange(temperature);
            		
                	etTemperature.setText(String.format("%.01f", temperature));
        			
        			pressure = getPressureGivenTemperature(temperature, refrigerants[currentRefrigerantIndex]);
        			if (pressure == Constants.PRESSURE_OUT_OF_BOUNDS){
        				displayErrorMessage(R.string.error_message_pressure);
        				return false;
        			}
        			
        			pressure = checkPressureIsWithinRange(pressure);
            		
        			//for pressure unit label
            		setPressureUnitLabelGivenPressure(pressure, tableName);  
            		
            		if (scPsig.isChecked())
            			pressure = Math.abs(pressure);
            		
        			etPressure.setText(String.format("%.02f", pressure));
            		
            		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
      		  		inputManager.hideSoftInputFromWindow(etTemperature.getWindowToken(), 0);
      		  		computeSuperHeat();
                }
				return false;
			}
		});
        etTemperature.setOnKeyListener(new OnKeyListener(){
        
            public boolean onKey(View v, int keyCode, KeyEvent event){
            	//
            	temperatureChanged = true;
            	isFromPressure = false;
            	//
                if (event.getAction()!=KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER){
                	float pressure = 0.0f;
                	float temperature = 0.0f;
                	
                	//get current refrigerant table name
            		String tableName = getCurrentRefrigerantTable();
            		
            		try{
            			temperature = parseReplace(etTemperature.getText().toString());//Float.parseFloat(etTemperature.getText().toString());
            		}catch(NumberFormatException e){
            			e.printStackTrace();
            		}
            		
                	temperature = checkTemperatureIsWithinRange(temperature);
            		
                	etTemperature.setText(String.format("%.01f", temperature));
        			
        			pressure = getPressureGivenTemperature(temperature, refrigerants[currentRefrigerantIndex]);
        			if (pressure == Constants.PRESSURE_OUT_OF_BOUNDS){
        				displayErrorMessage(R.string.error_message_pressure);
        				return false;
        			}
        			
        			pressure = checkPressureIsWithinRange(pressure);
            		
        			//for pressure unit label
            		setPressureUnitLabelGivenPressure(pressure, tableName);  
            		
            		if (scPsig.isChecked())
            			pressure = Math.abs(pressure);
            		
        			etPressure.setText(String.format("%.02f", pressure));
            		
            		InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE); 
      		  		inputManager.hideSoftInputFromWindow(etTemperature.getWindowToken(), 0);
      		  		computeSuperHeat();
                }
                return false;
            }
        });
        etbulbtemperature.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
			       if(actionId==EditorInfo.IME_ACTION_DONE){
	                	computeSuperHeat();
			       }
				return false;
			}
		});
        etbulbtemperature.setOnKeyListener(new OnKeyListener(){
            public boolean onKey(View v, int keyCode, KeyEvent event){
                if (event.getAction()!=KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER){
                	computeSuperHeat();
                }
                return false;
            }
        });
        
        scBarg.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		setTextViewValues();
        		View view = refrigerants[currentRefrigerantIndex].getView();
        		float pressureInBarg = 0.0f;
        		float pressureInPsig = 0.0f;
        		
        		//set self to non clickable to avoid re converting values
        		scBarg.setClickable(false);
        		scPsig.setClickable(true);
        		scPsig.setChecked(false);
        		
        		try{
        			pressureInPsig = parseReplace(etPressure.getText().toString());//Float.parseFloat(etPressure.getText().toString());
        			
        			if (isVacuum)
        				pressureInPsig = pressureInPsig * -1.0f;
        			
        			pressureInBarg = getPressureInBargGivenPressureInPsig(pressureInPsig);
        		}
        		catch(NumberFormatException e){
        			e.printStackTrace();
        		}
        		
        		pressureInBarg = checkPressureIsWithinRange(pressureInBarg);
        		
        		//set pressure unit labels
        		setPressureUnitLabelGivenPressure(pressureInBarg, getCurrentRefrigerantTable());
        		
        		//set pressure text
        		etPressure.setText(String.format("%.02f", pressureInBarg));
        		
        		//set min/max labels
        		//setMinMaxPressureLabel(view, getCurrentRefrigerantTable(), refrigerants[currentRefrigerantIndex]);
                Refrigerant r=  setMinMaxPressureLabel(refrigerants[currentRefrigerantIndex].getView(), getCurrentRefrigerantTable(), refrigerants[currentRefrigerantIndex]);
                refrigerants[currentRefrigerantIndex] = r;
                
                reloadViewPager();
                computeSuperHeat();
        	}
        });
        
        scPsig.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		setTextViewValues();
        		View view = refrigerants[currentRefrigerantIndex].getView();

        		float pressureInBarg = 0.0f;
        		float pressureInPsig = 0.0f;
        		
        		//set self to non clickable to avoid re converting values
        		scBarg.setClickable(true);
        		scPsig.setClickable(false);
        		scPsig.setChecked(true);
        		
        		try{
        			pressureInBarg = parseReplace(etPressure.getText().toString());//Float.parseFloat(etPressure.getText().toString());        		
        			pressureInPsig = getPressureInPsigGivenPressureInBarg(pressureInBarg);
        		}catch(NumberFormatException e){
        			e.printStackTrace();
        		}
        		
        		pressureInPsig = checkPressureIsWithinRange(pressureInPsig);
        		
        		//set pressure unit labels
        		setPressureUnitLabelGivenPressure(pressureInPsig, getCurrentRefrigerantTable());
        		
        		pressureInPsig = Math.abs(pressureInPsig);
        		
        		//set pressure text
        		etPressure.setText(String.format("%.02f", pressureInPsig));
        		
        		//set min/max labels
        		//setMinMaxPressureLabel(view, getCurrentRefrigerantTable(), refrigerants[currentRefrigerantIndex]);
                Refrigerant r=  setMinMaxPressureLabel(refrigerants[currentRefrigerantIndex].getView(), getCurrentRefrigerantTable(), refrigerants[currentRefrigerantIndex]);
                refrigerants[currentRefrigerantIndex] = r;
                
                reloadViewPager();
                computeSuperHeat();
        	}
        });
        
        scFahrenheit.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		setTextViewValues();
        		scCelsius.setClickable(true);
        		scFahrenheit.setClickable(false);
        		scFahrenheit.setChecked(true);
        		
        		View view = refrigerants[currentRefrigerantIndex].getView();
        		float temperatureInCelsius = 0.0f;
        		float temperatureInFahrenheit = 0.0f;
        		
        		try{
        			temperatureInCelsius = parseReplace(etTemperature.getText().toString());//Float.parseFloat(etTemperature.getText().toString());
        			temperatureInFahrenheit = getFahrenheitGivenCelsius(temperatureInCelsius);
        		}catch(NumberFormatException e){
        			e.printStackTrace();
        		}
        		
        		temperatureInFahrenheit = checkTemperatureIsWithinRange(temperatureInFahrenheit);
        		etTemperature.setText(String.format("%.01f", temperatureInFahrenheit));
        		
                //set min max
                Refrigerant r=  setMinMaxTemperatureLabel(view, refrigerants[currentRefrigerantIndex]);
                refrigerants[currentRefrigerantIndex] = r;
                reloadViewPager();
                computeSuperHeat();
        	}
        });
        
        scCelsius.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		setTextViewValues();
        		scFahrenheit.setClickable(true);
        		scCelsius.setClickable(false);
        		scCelsius.setChecked(true);
        		
        		View view = refrigerants[currentRefrigerantIndex].getView();
        		float temperatureInFahrenheit = 0.0f;
        		float temperatureInCelsius = 0.0f;
        		
        		try{
        			temperatureInFahrenheit = parseReplace(etTemperature.getText().toString());//Float.parseFloat(etTemperature.getText().toString()); 
        			temperatureInCelsius = getCelsiusGivenFahrenheit(temperatureInFahrenheit);
        		}catch(NumberFormatException e){
        			e.printStackTrace();
        		}
                
        		temperatureInCelsius = checkTemperatureIsWithinRange(temperatureInCelsius);
        		etTemperature.setText(String.format("%.01f", temperatureInCelsius));
                //set min max
                Refrigerant r=  setMinMaxTemperatureLabel(view, refrigerants[currentRefrigerantIndex]);
                refrigerants[currentRefrigerantIndex] = r;
                reloadViewPager();
                computeSuperHeat();
        	}
        });
        
        /*
        //on scroll change the pressurein psig given refrigerant
        hsScroll.setScrollViewListener(new ScrollViewListener(){
        	public void onScrollChanged(HorizontalScroll scrollView, int x, int y, int oldx, int oldy){
        		position = x;
        		if (oldx > x)
        			isGoingRight = false;
        		else
        			isGoingRight = true;
        		//currentRefrigerantIndex = getRefrigerantIndexGivenXPosition(x);
            	//setCurrentRefrigerantValues();
            	//Log.v("avian", String.valueOf(isGoingRight));
        	}
        });
        */
        
        
         hsScroll.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {	
        	 public void onPageSelected(int pageindex) {
        		
        		currentRefrigerantIndex = pageindex;
        		setTextViewValues();
 				setCurrentRefrigerantValues();
 				computeSuperHeat();
 				
 				cdt = new CountDownTimer(600,300) {
        			public void onTick(long millisUntilFinished) {
        				hsScroll.setPagingEnabled(false);
        			}
        			public void onFinish() {
        				hsScroll.setPagingEnabled(true);
        			}
        		};
        		cdt.start();

			
        	 }
			
			public void onPageScrolled(int arg0, float arg1, int arg2) {
					
			}
			
			public void onPageScrollStateChanged(int arg0) {

			}
		});
         
         
		  
         
        buttonScrollLeft.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		cdt = new CountDownTimer(400,400) {
        			public void onTick(long millisUntilFinished) {
        				buttonScrollLeft.setEnabled(false);
        				buttonScrollRight.setEnabled(false);
        				setTextViewValues();
                		if (currentRefrigerantIndex > 0)
                		{
        	        		currentRefrigerantIndex = currentRefrigerantIndex - 1;
        	         		hsScroll.setCurrentItem(currentRefrigerantIndex);
        	        		setCurrentRefrigerantValues();
        	        	}
        			}
        			public void onFinish() {
        				buttonScrollLeft.setEnabled(true);
        				buttonScrollRight.setEnabled(true);
        			}
        		};
        		cdt.start();
        		
        	}
        });
        
        buttonScrollRight.setOnClickListener(new OnClickListener(){
        	public void onClick(View v){
        		
        		cdt = new CountDownTimer(400,400) {
        			public void onTick(long millisUntilFinished) {
        				buttonScrollRight.setEnabled(false);
        				buttonScrollLeft.setEnabled(false);
        				setTextViewValues();
                		if (currentRefrigerantIndex  < refrigerants.length - 1)
                		{
        	         		currentRefrigerantIndex = currentRefrigerantIndex + 1;
        	         		hsScroll.setCurrentItem(currentRefrigerantIndex);
        	        		setCurrentRefrigerantValues();
                		}
        			}
        			public void onFinish() {
        				buttonScrollRight.setEnabled(true);
        				buttonScrollLeft.setEnabled(true);
        			}
        		};
        		cdt.start();
        	}
        });
        
        
 		//localyticsSession.setLoggingEnabled(true);
        
    }
    
    @Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
	}
    
    @Override
    public void onDestroy(){
    	super.onDestroy();
    	dbHelper.close();
    }
    
    //show dialog
	private void showDialog() {
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(R.string.alert_title);
		builder.setMessage(R.string.app_license_agreement);
		builder.setPositiveButton(R.string.eula_agree, new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	        	   sharedPreferencesEditor.putBoolean(Constants.SHARED_PREFERENCES_BOOLEAN_VALUE, true);
	        	   sharedPreferencesEditor.commit();
	        	   dialog.cancel();
	           }
	    });
		builder.setNegativeButton(R.string.eula_disagree, new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	        	   sharedPreferencesEditor.putBoolean(Constants.SHARED_PREFERENCES_BOOLEAN_VALUE, false);
	        	   sharedPreferencesEditor.commit();
	        	   finish();
	           }
	    });
		AlertDialog alert = builder.create();
		alert.show();
	}
	

	private void computeSuperHeat()
	{
    	float temperature = 0.0f;
    	float bulbtemperature = 0.0f;
    	try{
			temperature = parseReplace(etTemperature.getText().toString());//Float.parseFloat(etTemperature.getText().toString());
			bulbtemperature = parseReplace(etbulbtemperature.getText().toString());//Float.parseFloat(etbulbtemperature.getText().toString());
		}catch(NumberFormatException e){
			e.printStackTrace();
		}
		
		etsuperheat.setText("");
		if ((etbulbtemperature.getText().toString().trim().length() > 0) && (etTemperature.getText().toString().trim().length() > 0))
		{
			if (bulbtemperature > temperature)
				etsuperheat.setText(String.format("%.1f", bulbtemperature - temperature));
		}
	}
	
    //this method sets the default values
    private void setDefaultValues(){
    	isSeaLevel = true;
    	isVacuum = false;
    	scFahrenheit.setChecked(true);												//is Fahrenheit = true
		scPsig.setChecked(true);													//is Psig = true
		txtSeaLevel.setText(getString(R.string.main_sealevel));	//set default image for above sea level indicator button
    	currentRefrigerantIndex = 0;												//current refrigerant's pointer
    	//buttonScrollLeft.setVisibility(View.INVISIBLE);
    	
    	//create refrigerants
    	for (int index = 0; index < Refrigerant.REFRIGERANTS_COUNT; index++){
    		refrigerants[index] = addRefrigerantInHorizontalScrollView(index);
    	}
    	
    	String tableName = getCurrentRefrigerantTable();
    	
    	//set the default values of pressure and temp edit texts
    	float pressureInPsig = getPressureGivenTemperature(refrigerants[currentRefrigerantIndex].getMinTempF(), refrigerants[currentRefrigerantIndex]);
    	    	
    	etTemperature.setText(String.format("%.01f", refrigerants[currentRefrigerantIndex].getMinTempF()));
    	
    	//for pressure unit label
		setPressureUnitLabelGivenPressure(pressureInPsig, tableName);
		
		if (isVacuum)
			pressureInPsig = Math.abs(pressureInPsig);
		
    	etPressure.setText(String.valueOf(pressureInPsig));
    }
    
    private float checkPressureIsWithinRange(float pressure){
    	String tableName = getCurrentRefrigerantTable();
    	//float minTemp = Constants.MIN_TEMP_IN_FAHRENHEIT;
		//float maxTemp = Constants.MAX_TEMP_IN_FAHRENHEIT;
		
		float minTemp = refrigerants[currentRefrigerantIndex].getMinTempF();
		float maxTemp = refrigerants[currentRefrigerantIndex].getMaxTempF();
		
		if (!scFahrenheit.isChecked()){
			//minTemp = Constants.MIN_TEMP_IN_CELSIUS;
			//maxTemp = Constants.MAX_TEMP_IN_CELSIUS;
			minTemp = refrigerants[currentRefrigerantIndex].getMinTempC();
			maxTemp = refrigerants[currentRefrigerantIndex].getMaxTempC();
		}
		
		float minPressure = getPressureGivenTemperature(minTemp, refrigerants[currentRefrigerantIndex]);
		float maxPressure = getPressureGivenTemperature(maxTemp, refrigerants[currentRefrigerantIndex]);
		
		if (pressure < minPressure)
			pressure = minPressure;
		else if (pressure > maxPressure)
			pressure = maxPressure;
		
		return pressure;
    }
    
    private float checkTemperatureIsWithinRange(float temperature){
    	//float minTemp = Constants.MIN_TEMP_IN_FAHRENHEIT;
		//float maxTemp = Constants.MAX_TEMP_IN_FAHRENHEIT;
		float minTemp = refrigerants[currentRefrigerantIndex].getMinTempF();
		float maxTemp = refrigerants[currentRefrigerantIndex].getMaxTempF();
		
		if (!scFahrenheit.isChecked()){
			//minTemp = Constants.MIN_TEMP_IN_CELSIUS;
			//maxTemp = Constants.MAX_TEMP_IN_CELSIUS;
			minTemp = refrigerants[currentRefrigerantIndex].getMinTempC();
			maxTemp = refrigerants[currentRefrigerantIndex].getMaxTempC();
		}
		
		if (temperature < minTemp)
			temperature = minTemp;
		else if (temperature > maxTemp)
			temperature = maxTemp;
		
		return temperature;
    }
    
    //this method set the refrigerant values in current refrigerant
    private void setCurrentRefrigerantValues(){
    	
    	// set buttons
    	/*
    	if (currentRefrigerantIndex >= (refrigerants.length - 1))
    		this.buttonScrollRight.setVisibility(View.INVISIBLE);
    	else
    		this.buttonScrollRight.setVisibility(View.VISIBLE);
    	
    	if (currentRefrigerantIndex <= 0)
    		this.buttonScrollLeft.setVisibility(View.INVISIBLE);
    	else
    		this.buttonScrollLeft.setVisibility(View.VISIBLE);
    	*/
    	tvCurrentPage.setText(String.valueOf(currentRefrigerantIndex + 1) + " of " + String.valueOf(refrigerants.length));
    	
    	View view = refrigerants[currentRefrigerantIndex].getView();
    	
    	//set pressure edit text and min/max

    	if ((temperatureChanged) == false && (pressureChanged == false))
    	{
    		float pressure = getPressureGivenTemperature(refrigerants[currentRefrigerantIndex].getMinTempF(), refrigerants[currentRefrigerantIndex]);
    		if (scCelsius.isChecked())
    			pressure = getPressureGivenTemperature(refrigerants[currentRefrigerantIndex].getMinTempC(), refrigerants[currentRefrigerantIndex]);

        	//set pressure unit labels
    		setPressureUnitLabelGivenPressure(pressure, getCurrentRefrigerantTable());
    		
    		if (scPsig.isChecked())
    			pressure = Math.abs(pressure);
    		
        	etPressure.setText(String.format("%.02f", pressure));
            
        	//set temperature edit text and min/max
        	if (scFahrenheit.isChecked()){
        		etTemperature.setText(String.format("%.01f", refrigerants[currentRefrigerantIndex].getMinTempF()));
        	}
        	else{
        		etTemperature.setText(String.format("%.01f", refrigerants[currentRefrigerantIndex].getMinTempC()));
        	}
    	}
    	else
    	{
    		if (isFromPressure == true)
    		{
    			float temperature = Constants.MIN_TEMP_IN_FAHRENHEIT;
            	float pressure = 0.0f;
            	
            	//get current refrigerant
        		String tableName = getCurrentRefrigerantTable();
        		
        		//for temperature edit text
        		try{
        			pressure = parseReplace(etPressure.getText().toString());//Float.parseFloat(etPressure.getText().toString());            			
        		}catch(NumberFormatException e){
        			e.printStackTrace();
        		}
        		
        		pressure = checkPressureIsWithinRange(pressure);
    			
    			temperature = getTemperatureGivenPressure(pressure, tableName);
    			
    			temperature = checkTemperatureIsWithinRange(temperature);
    			
    			etTemperature.setText(String.format("%.01f", temperature));
    			
    			//for pressure unit label
        		setPressureUnitLabelGivenPressure(pressure, tableName);

    			if (scPsig.isChecked())
    				pressure = Math.abs(pressure);
    			
    			etPressure.setText(String.format("%.02f", pressure));
    		}
    		else
    		{
    			float pressure = 0.0f;
            	float temperature = 0.0f;
            	
            	//get current refrigerant table name
        		String tableName = getCurrentRefrigerantTable();
        		
        		try{
        			temperature = parseReplace(etTemperature.getText().toString());//Float.parseFloat(etTemperature.getText().toString());
        		}catch(NumberFormatException e){
        			e.printStackTrace();
        		}
        		
            	temperature = checkTemperatureIsWithinRange(temperature);
        		
            	etTemperature.setText(String.format("%.01f", temperature));
    			
    			pressure = getPressureGivenTemperature(temperature, refrigerants[currentRefrigerantIndex]);
    			if (pressure == Constants.PRESSURE_OUT_OF_BOUNDS){
    				displayErrorMessage(R.string.error_message_pressure);
    			}
    			else
    			{
	    			pressure = checkPressureIsWithinRange(pressure);
	        		
	    			//for pressure unit label
	        		setPressureUnitLabelGivenPressure(pressure, tableName);  
	        		
	        		if (scPsig.isChecked())
	        			pressure = Math.abs(pressure);
	        		
	    			etPressure.setText(String.format("%.02f", pressure));
    			}
    		}
    	}

    	// refreshes min max range per swipe
        Refrigerant r=  setMinMaxPressureLabel(refrigerants[currentRefrigerantIndex].getView(), getCurrentRefrigerantTable(), refrigerants[currentRefrigerantIndex]);
        refrigerants[currentRefrigerantIndex] = r;
        r =  setMinMaxTemperatureLabel(view, refrigerants[currentRefrigerantIndex]);
        refrigerants[currentRefrigerantIndex] = r;
        reloadViewPager();
    }
    
    //this method get the current Refrigerant table
    private String getCurrentRefrigerantTable(){
    	return refrigerants[currentRefrigerantIndex].getTableName();
    }
    
    //this method get the refrigerant table given the position of current refrigerant display
    /*
    public int getRefrigerantIndexGivenXPosition(int xPosition){
    	int previousContainerWidth = 0, halfContainerWidth = 0, containerWidth = 0;
    	
    	containerWidth = width-10;							//10 is for total padding, 5 on each side
    	halfContainerWidth = (containerWidth)/2;
    	previousContainerWidth = containerWidth+halfContainerWidth;
    	
    	if (xPosition == 0)
    		buttonScrollLeft.setVisibility(View.INVISIBLE);
    	
    	if (xPosition <= previousContainerWidth+((containerWidth*17)+halfContainerWidth/2-20))
    		buttonScrollRight.setVisibility(View.VISIBLE);
    	else 
    		buttonScrollRight.setVisibility(View.INVISIBLE);
    	
    	if (xPosition <= halfContainerWidth){
    		buttonScrollRight.setVisibility(View.VISIBLE);
    		return Refrigerant.R410A;
    	} else if (xPosition >= halfContainerWidth && xPosition <= previousContainerWidth){
    		buttonScrollLeft.setVisibility(View.VISIBLE);
    		return Refrigerant.R12;
    	} else if (xPosition >= previousContainerWidth+1 && xPosition <= previousContainerWidth+containerWidth){
    		return Refrigerant.R22;
    	} else if (xPosition >= previousContainerWidth+(containerWidth+1) && xPosition <= previousContainerWidth+(containerWidth*2)){
    		return Refrigerant.R134A;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*2)+1) && xPosition <= previousContainerWidth+(containerWidth*3)){
    		return Refrigerant.R404A;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*3)+1) && xPosition <= previousContainerWidth+(containerWidth*4)){
    		return Refrigerant.R407AV;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*4)+1) && xPosition <= previousContainerWidth+(containerWidth*5)){
    		return Refrigerant.R407AL;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*5)+1) && xPosition <= previousContainerWidth+(containerWidth*6)){
    		return Refrigerant.R407CV;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*6)+1) && xPosition <= previousContainerWidth+(containerWidth*7)){
    		return Refrigerant.R407CL;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*7)+1) && xPosition <= previousContainerWidth+(containerWidth*8)){
    		return Refrigerant.R408A;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*8)+1) && xPosition <= previousContainerWidth+(containerWidth*9)){
    		return Refrigerant.R409A;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*9)+1) && xPosition <= previousContainerWidth+(containerWidth*10)){
    		return Refrigerant.R401A;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*10)+1) && xPosition <= previousContainerWidth+(containerWidth*11)){
    		return Refrigerant.R502;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*11)+1) && xPosition <= previousContainerWidth+(containerWidth*12)){
    		return Refrigerant.R507A;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*12)+1) && xPosition <= previousContainerWidth+(containerWidth*13)){
    		return Refrigerant.R600;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*13)+1) && xPosition <= previousContainerWidth+(containerWidth*14)){
    		return Refrigerant.R290;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*14)+1) && xPosition <= previousContainerWidth+(containerWidth*15)){
    		return Refrigerant.RCO2;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*15)+1) && xPosition <= previousContainerWidth+(containerWidth*16)){
    		return Refrigerant.R407F;
    	} else if (xPosition >= previousContainerWidth+((containerWidth*16)+1) && xPosition <= previousContainerWidth+(containerWidth*17)){
    		return Refrigerant.R402A;
    	} else {
    		return Refrigerant.R717;
    	}
    }
    */
    //this method sets the pressure state labels
    public void setPressureUnitLabelGivenPressure(float pressure, String tableName){
    	
    	String unit = null, state = null, unitState = null;
    	int color = 0, isBold = 0;
    	float pressureInPsig = pressure;
    	
    	if (scPsig.isChecked()){
    		if(isPressureInPsigVacuum(pressureInPsig))
    			unit = getResources().getString(R.string.unithg);
    		else
    			unit = getResources().getString(R.string.unitpsig)+ "       ";
    	}
    	else{
    		pressureInPsig = this.getPressureInPsigGivenPressureInBarg(pressure);
    		unit = getResources().getString(R.string.unitbarg)+ "       ";
    	}
    	
    	if (isPressureInPsigVacuum(pressureInPsig)){
    		if(scPsig.isChecked())
    			state = getResources().getString(R.string.vacuum);
    		else
    			state = "      "+getResources().getString(R.string.vacuum);
    		
    		color = Color.RED;
    		isBold = 0;
    		isVacuum = true;
    	}
    	else if (isPressureInPsigLiquid(pressureInPsig, tableName)){
    		state =  "      "+getResources().getString(R.string.liquid);
    		color = Color.BLACK;
    		isBold = 1;
    		isVacuum = false;
    	}
    	else{
    		state = "         "+getResources().getString(R.string.vapor);
    		color = Color.BLACK;
    		isBold = 0;
    		isVacuum = false;
    	}
    	
    	
    	unitState = state + " " + unit;
    	tvPressureUnit.setTextColor(color);
    	tvPressureUnit.setTypeface(null, isBold);
    	tvPressureUnit.setText(unitState);
    }
    
    public float getTemperatureGivenPressure(float pressure, String tableName){
    	Cursor mCursor = null;
    	float lPressure = 0, hPressure = 0, lTemp = 0, hTemp = 0, tempInF = 0, tempInC = 0;
    	int noOfRows = 0;
    	
    	mCursor = dbHelper.getCursorGivenExactPressure(pressure, tableName);
    	
    	if (mCursor != null){
    		noOfRows = mCursor.getCount(); 
		}
		else{
			displayErrorMessage(R.string.db_error);
			return 0;
		}
    	
    	if (noOfRows == 1){
    		tempInF = getTemperatureInFahrenheitGivenCursor(mCursor);
    	}
    	else{
    		mCursor = dbHelper.getCursorForLowerRangePressure(pressure, tableName);
    		noOfRows = 0;        // set reset
    		if (mCursor != null){
        		noOfRows = mCursor.getCount();    		
    			if (noOfRows == 0)
    				return Constants.TEMP_OUT_OF_BOUNDS;
    		}
    		else{
    			displayErrorMessage(R.string.db_error);
    			return 0;
    		}
    		
    		lTemp = getTemperatureInFahrenheitGivenCursor(mCursor);
    		lPressure = getPressureInPsigGivenCursor(mCursor);
    		
    		mCursor = dbHelper.getCursorForHigherRangePressure(pressure, tableName);
    		noOfRows = 0;        // set reset
    		if (mCursor != null){
        		noOfRows = mCursor.getCount();    		
    			if (noOfRows == 0)
    				return Constants.TEMP_OUT_OF_BOUNDS;
    		}
    		else{
    			displayErrorMessage(R.string.db_error);
    			return 0;
    		}
    		hTemp = getTemperatureInFahrenheitGivenCursor(mCursor);
    		hPressure = getPressureInPsigGivenCursor(mCursor);
    		
    		if (scBarg.isChecked())
    			pressure = this.getPressureInPsigGivenPressureInBarg(pressure);
    		
    		tempInF = getTemperatureGivenPressureUsingLinearInterpolation(pressure, lPressure, hPressure, lTemp, hTemp);
    		
    	}
    	
    	tempInC = this.getCelsiusGivenFahrenheit(tempInF);
    	
    	if (scFahrenheit.isChecked())
    		return tempInF;
    	else
    		return tempInC;
    }
    
    public float getPressureGivenTemperature(float temperature, Refrigerant r){
    	Cursor mCursor = null;
    	float lPressure = 0, hPressure = 0, lTemp = 0, hTemp = 0, pressureInPsig = 0, pressureInBarg = 0;
    	int noOfRows = 0;
    	
    	mCursor = dbHelper.getCursorGivenExactTemperature(temperature, r.getTableName());
    	
    	if (mCursor != null){
    		noOfRows = mCursor.getCount();    		
		}
		else{
			displayErrorMessage(R.string.db_error);
			return 0;
		}
    	
    	if (noOfRows == 1){
    		pressureInPsig = getPressureInPsigGivenCursor(mCursor);
    	}
    	else{
    		mCursor = dbHelper.getCursorForLowerRangeTemperature(temperature, r.getTableName());
    		noOfRows = 0;        // set reset
    		if (mCursor != null){
        		noOfRows = mCursor.getCount();    		
    			if (noOfRows == 0)
    				return Constants.PRESSURE_OUT_OF_BOUNDS;
    		}
    		else{
    			displayErrorMessage(R.string.db_error);
    			return 0;
    		}    		
    		lTemp = getTemperatureInFahrenheitGivenCursor(mCursor);
    		lPressure = getPressureInPsigGivenCursor(mCursor);
    		
    		mCursor = dbHelper.getCursorForHigherRangeTemperature(temperature, r.getTableName());
    		if (mCursor != null){
        		noOfRows = mCursor.getCount();    		
    			if (noOfRows == 0)
    				return Constants.PRESSURE_OUT_OF_BOUNDS;
    		}
    		else{
    			displayErrorMessage(R.string.db_error);
    			return Constants.PRESSURE_OUT_OF_BOUNDS;
    		}
    		hTemp = getTemperatureInFahrenheitGivenCursor(mCursor);
    		hPressure = getPressureInPsigGivenCursor(mCursor);
    		
    		
    		//if (hTemp > Constants.MAX_TEMP_IN_FAHRENHEIT)
    		if (hTemp > r.getMaxTempF())
    		return Constants.PRESSURE_OUT_OF_BOUNDS; 		
    		//else if (lTemp < Constants.MIN_TEMP_IN_FAHRENHEIT)
    		else if (lTemp < r.getMinTempF())
    			return Constants.PRESSURE_OUT_OF_BOUNDS;
    		else{    			
    			if (scCelsius.isChecked())
    				temperature = this.getFahrenheitGivenCelsius(temperature);

    			if ((isPressureInPsigVapor(hPressure, r.getTableName()) && isPressureInPsigVapor(lPressure, r.getTableName())) || 
    				(isPressureInPsigLiquid(hPressure,r.getTableName()) && isPressureInPsigLiquid(lPressure, r.getTableName())) ||
    				(isPressureInPsigVacuum(hPressure) && isPressureInPsigVacuum(lPressure))){
    				pressureInPsig = getPressureGivenTemperatureUsingLinearInterpolation(temperature, lPressure, hPressure, lTemp, hTemp);
    			}
    			else{
    				if (isPressureInPsigVacuum(hPressure) || isPressureInPsigVacuum(lPressure)){
    					
    					float hPressureInPsia = getPressureInPsiaGivenPressureInPsig(hPressure);
    					float lPressureInPsia = getPressureInPsiaGivenPressureInPsig(lPressure);
    					
    					float pressureInPsia = getPressureGivenTemperatureUsingLinearInterpolation(temperature, lPressureInPsia, hPressureInPsia, lTemp, hTemp);
    					
    					pressureInPsig = getPressureInPsigGivenPressureInPsia(pressureInPsia);
    				}
    				else{
    					return Constants.PRESSURE_OUT_OF_BOUNDS;
    				}
    			}
    		}
    	}
    	
    	pressureInBarg = getPressureInBargGivenPressureInPsig(pressureInPsig);    					
		if (scPsig.isChecked())
			return pressureInPsig;
		else
			return pressureInBarg;
    }
    
    //this method returns true given the pressure in psig, if it is liquid
    public boolean isPressureInPsigLiquid(float pressure, String tableName){
		if (isSeaLevel){
			if (tableName.equals(refrigerants[Refrigerant.R407AL].getTableName()) && pressure >= 0.0f)
				 return true;
			else if(tableName.equals(refrigerants[Refrigerant.R404A].getTableName()) && pressure >= 107.5f)
				return true;
			else if(tableName.equals(refrigerants[Refrigerant.R407CL].getTableName()) && pressure >= 0.0f)
				return true;
			else if(tableName.equals(refrigerants[Refrigerant.R409A].getTableName()) && pressure >= 46.5f)
				return true;
		}
		else{
			if (tableName.equals(refrigerants[Refrigerant.R407AL].getTableName()) && pressure >= 0.0f)
				return true;
			else if(tableName.equals(refrigerants[Refrigerant.R404A].getTableName()) && pressure >= 109.9f)
				return true;
			else if(tableName.equals(refrigerants[Refrigerant.R407CL].getTableName()) && pressure >= 0.0f)
				return true;
			else if(tableName.equals(refrigerants[Refrigerant.R409A].getTableName()) && pressure >= 48.9f)
				return true;
		}
    	
    	return false;
    }
    
    //this method returns true given the pressure in psig, if it is vacuum
    public boolean isPressureInPsigVacuum(float pressure){    	
    	if (pressure < 0)
    		return true;   	
    	
    	return false;
    }
    
    //this method returns true given the pressure in psig, if it is vapor
    public boolean isPressureInPsigVapor(float pressure, String tableName){
    	if (!isPressureInPsigVacuum(pressure) && !isPressureInPsigLiquid(pressure, tableName))
    		return true;
    	
    	return false;
    }
    
    //this method returns cursor pressure in psig given a cursor
    public float getPressureInPsigGivenCursor(Cursor mCursor){
    	if (isSeaLevel)
			return mCursor.getFloat(mCursor.getColumnIndex(EmersonPtDBadapter.COLUMN_PRESSURE_SEALEVEL_PSIG));
		else
			return mCursor.getFloat(mCursor.getColumnIndex(EmersonPtDBadapter.COLUMN_PRESSURE_ABOVESEALEVEL_PSIG));
    }
    
    public float getTemperatureInFahrenheitGivenCursor(Cursor mCursor){
    	return mCursor.getFloat(mCursor.getColumnIndex(EmersonPtDBadapter.COLUMN_TEMPERATURE_FAHRENHEIT));
    }
    
    //this method returns celsius given fahrenheit
    public float getCelsiusGivenFahrenheit(float fahrenheit){
    	float celsius = 5.0f / 9.0f * (fahrenheit - 32.0f);
    	return celsius;
    }
    
    //this method returns fahrenheit given celsius
    public float getFahrenheitGivenCelsius(float celsius){
    	float fahrenheit = 9.0f / 5.0f * celsius + 32.0f;
    	
    	String tableName = getCurrentRefrigerantTable();
    	int noOfRows = 0;
    	
    	Cursor mCursor = dbHelper.getCursorGivenExactTemperatureInCelsius(celsius, tableName);
    	
    	if (mCursor != null){
    		noOfRows = mCursor.getCount();    		
			if (noOfRows > 0){
				fahrenheit = mCursor.getFloat(mCursor.getColumnIndex(EmersonPtDBadapter.COLUMN_TEMPERATURE_FAHRENHEIT));
			}
		}
		else{
			displayErrorMessage(R.string.db_error);
			return 0;
		}
    	
    	return fahrenheit;
    }
    
    //this method returns temperature given pressure using the Linear Interpolation
    public float getTemperatureGivenPressureUsingLinearInterpolation(float pressure, float lPressure, float hPressure, float lTemp, float hTemp){
    	return (((pressure - lPressure) * (hTemp - lTemp)) / (hPressure - lPressure)) + lTemp;
    	
    }
    
    //this method returns pressure given temperature using the Linear Interpolation
    public float getPressureGivenTemperatureUsingLinearInterpolation(float temperature, float lPressure, float hPressure, float lTemp, float hTemp){
    	return (((temperature - lTemp) * (hPressure - lPressure)) / (hTemp - lTemp)) + lPressure;
    }
    
    //this method returns pressure in psia given prssure in psig
    public float getPressureInPsiaGivenPressureInPsig(float pressureInPsig){
    	float pressureInPsia = 0;
    	float absPressureInPsig = Math.abs(pressureInPsig);
    	
    	if (isPressureInPsigVacuum(pressureInPsig))
    		pressureInPsia = 14.7f - (absPressureInPsig * 0.49f);
    	else
    		pressureInPsia = absPressureInPsig + 14.7f;
    	
    	return pressureInPsia;
    }
    
    //this method returns pressure in psig given pressure in psia
    public float getPressureInPsigGivenPressureInPsia(float pressureInPsia){
    	float pressureInPsig = 0;
    	if (pressureInPsia >= 14.7f)
    		pressureInPsig = pressureInPsia - 14.7f;
    	else{
    		pressureInPsig = ((14.7f - pressureInPsia) / 14.7f) * 29.92f;
    		pressureInPsig = pressureInPsig * -1.0f;
    	}
    	
    	return pressureInPsig;
    }
    
    //this method returns pressure in barg given pressure in psig
    public float getPressureInBargGivenPressureInPsig(float pressureInPsig){
    	float bPressure = 0;
        
        if (isPressureInPsigVacuum(pressureInPsig)){
            bPressure = ((pressureInPsig/29.92f) * 14.7f)/14.5f;
        }
        else{
            bPressure = pressureInPsig / 14.5f;
        }
        
        return bPressure;
    }
    
    //this method returns pressure in psig given pressure in barg
    public float getPressureInPsigGivenPressureInBarg(float pressureInBarg){
    	float pPressure = 0;
    	String tableName = getCurrentRefrigerantTable();
    	int noOfRows = 0;
    	
    	Cursor mCursor = dbHelper.getCursorGivenExactPressureInBarg(pressureInBarg, tableName);
    	
    	if (mCursor != null){
    		noOfRows = mCursor.getCount();    		
			if (noOfRows == 0){
				if (isPressureInPsigVacuum(pressureInBarg)){
		            pPressure = ((pressureInBarg * 14.5f) / 14.7f) * 29.92f;
		        }
		        else{
		            pPressure = pressureInBarg * 14.5f;
		        }
			}
			else{
				if (isSeaLevel)
					pPressure = mCursor.getFloat(mCursor.getColumnIndex(EmersonPtDBadapter.COLUMN_PRESSURE_SEALEVEL_PSIG));
				else
					pPressure = mCursor.getFloat(mCursor.getColumnIndex(EmersonPtDBadapter.COLUMN_PRESSURE_ABOVESEALEVEL_PSIG));
			}
		}
		else{
			displayErrorMessage(R.string.db_error);
			return 0;
		}
        
        return pPressure;
    }
    //HERE!
    //this method intialize all the components
    private void initLayout() {       
    	buttonScrollLeft = (Button) findViewById(R.id.scrollleft);
    	buttonScrollRight = (Button) findViewById(R.id.scrollright);
    	btnSeaLevel = (LinearLayout) findViewById(R.id.ll_abovesealevel);
    	txtSeaLevel= (TextView) findViewById(R.id.txt_abovesealevel);
        ibtnInfo = (ImageButton) findViewById(R.id.info);
        ivLogo = (ImageView) findViewById(R.id.logo);
        tvPressureUnit = (TextView) findViewById(R.id.unit);
        tvSealevel = (TextView) findViewById(R.id.sealevel);
        //tvRefrigerantbar = (TextView) findViewById(R.id.refrigerantbar);
        etPressure = (EditText) findViewById(R.id.pressure);
        etTemperature = (EditText) findViewById(R.id.temperature);
        etbulbtemperature = (EditText) findViewById(R.id.bulbtemperature);
        etsuperheat = (TextView) findViewById(R.id.superheat);
        tvCurrentPage = (TextView)findViewById(R.id.textCurrentPage);
        llIconsrow = (LinearLayout) findViewById(R.id.iconsRow);
        //hsScroll = (HorizontalScroll) findViewById(R.id.scrollView);
        hsScroll = (CustomViewPager) findViewById(R.id.scrollView);
        scPsig = (SegmentedControl) findViewById(R.id.psig);
        scBarg = (SegmentedControl) findViewById(R.id.bar);
        scFahrenheit = (SegmentedControl) findViewById(R.id.farenheit);
        scCelsius = (SegmentedControl) findViewById(R.id.celsius);
        llSegmentedRow = (LinearLayout) findViewById(R.id.segmented_togglebutton_main);
        llEditTextRow = (LinearLayout) findViewById(R.id.etrow);
        //hsvParentLinearLayout = (LinearLayout) findViewById(R.id.scrollViewLinearLayoutParent);
    	vi = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
    
    //this method set a padding
    private void setLayoutPadding() {		
    	/*
		display = getWindowManager().getDefaultDisplay(); 
        width = display.getWidth();
        int height = display.getHeight();
        
        int paddingWidth = width/32;
        
    	ivLogo.setPadding(paddingWidth*3, 0, paddingWidth*5, 0);
    	//tvRefrigerantbar.setPadding(paddingWidth*7, 0, paddingWidth*7, 0);
    	
    	if (width >= 480 && height >=800 ){
    		llEditTextRow.setPadding(0, 0, 0, (height/24)/2);
    		llIconsrow.setPadding(0, height/24, 0, height/36);
    		llSegmentedRow.setPadding(0, 0, 0, (height/40)-5);
    		hsScroll.setPadding(paddingWidth/2, 5, paddingWidth/2, 5);
    	}else{
    		llIconsrow.setPadding(0, height/50, 0, (height/49)/2);
    		llSegmentedRow.setPadding(0, 0, 0, 0);
    		hsScroll.setPadding(paddingWidth/2, 2, paddingWidth/2, 2);
    	}
    	*/
	}
    
    //this method add refrigerant views in horizontal scroll view
    private Refrigerant addRefrigerantInHorizontalScrollView(int index){ 
    	View view = vi.inflate(R.layout.refrigerant, null);
    	ImageView ivRefrigerant = (ImageView) view.findViewById(R.id.refrigerantImage);
    	TextView tvRefrigerantName = (TextView) view.findViewById(R.id.refrigerantName);
    	
    	Refrigerant refrigerant = new Refrigerant(view, index);
    	refrigerant.setMaxTempF(dbHelper.getMaxTempF(refrigerant.getTableName()));
    	refrigerant.setMaxTempC(dbHelper.getMaxTempC(refrigerant.getTableName()));
    	refrigerant.setMinTempF(dbHelper.getMinTempF(refrigerant.getTableName()));
    	refrigerant.setMinTempC(dbHelper.getMinTempC(refrigerant.getTableName()));
		
    	ivRefrigerant.setImageResource(refrigerant.getImage());
    	tvRefrigerantName.setText(refrigerant.getName());
    	setMinMaxTemperatureLabel(view, refrigerant);
    	setMinMaxPressureLabel(view, refrigerant.getTableName(), refrigerant);
    	
    	//hsvParentLinearLayout.addView(view);
    	refrigerant.setView(view);
    	return refrigerant;
    }
    
    //this method set temperature Minimum and Maximum of given refrigerant
    public Refrigerant setMinMaxTemperatureLabel(View view, Refrigerant r){
    	TextView tvMinTemperature = (TextView) view.findViewById(R.id.tempmin);
    	TextView tvMaxTemperature = (TextView) view.findViewById(R.id.tempmax);
    	
    	if (scFahrenheit.isChecked()){
    		tvMinTemperature.setText(getString(R.string.refrigerant_min)+":       "+String.valueOf(r.getMinTempF()));
    		tvMaxTemperature.setText(getString(R.string.refrigerant_max)+":       "+String.valueOf(r.getMaxTempF()));
    	}
    	else{
    		tvMinTemperature.setText(getString(R.string.refrigerant_min)+":       "+String.valueOf(r.getMinTempC()));
    		tvMaxTemperature.setText(getString(R.string.refrigerant_max)+":       "+String.valueOf(r.getMaxTempC()));
    	}
    	r.setView(view);
    	return r;
    }
    
    //this method set temperature Minimum and Maximum of given refrigerant
    public Refrigerant setMinMaxPressureLabel(View view, String tableName, Refrigerant r){
    	/*
    	 * Above sea level values are determined inside getPressureInPsigGivenTemperatureInFahrenheit
    	 */
    	TextView tvMinPressure = (TextView) view.findViewById(R.id.pressuremin);
    	TextView tvMaxPressure = (TextView) view.findViewById(R.id.pressuremax);
    	
    	float minTemp = r.getMinTempF();
    	float maxTemp = r.getMaxTempF();
    	
    	if (scCelsius.isChecked()){
    		minTemp = r.getMinTempC();
        	maxTemp =r.getMaxTempC();
    	}
    	
    	float minPressure = getPressureGivenTemperature(minTemp, r);
    	float maxPressure = getPressureGivenTemperature(maxTemp, r);
    	
    	if (scPsig.isChecked()){
    		minPressure = Math.abs(minPressure);
    		maxPressure = Math.abs(maxPressure);
    	}
    	
    	tvMinPressure.setText(getString(R.string.refrigerant_min)+":       "+String.format("%.02f", minPressure));
		tvMaxPressure.setText(getString(R.string.refrigerant_max)+":       "+String.format("%.02f", maxPressure));
		
    	r.setView(view);
    	return r;
    }    
    
    // reload view
    private void reloadViewPager()
    {
    	this.hsScroll.getAdapter().notifyDataSetChanged();
    }
    //this method display a error message
    private void displayErrorMessage(int errorMessage) {
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Error");
		builder.setMessage(errorMessage);
		AlertDialog alert = builder.create();
		alert.show();
    }
    
	//database helper and methods
	public class EmersonPtDBadapter extends SQLiteOpenHelper{
		
		//declare static variables
		static final String TAG = "PTCHART";
		public final String DATABASE_PATH = Environment.getDataDirectory() + "/data/com.emersonclimate.PTPro/databases/";
		public static final String DATABASE_NAME = "PTPro.sqlite";		
		public static final String COLUMN_ID = "rowid";
		public static final String COLUMN_TEMPERATURE_CELSIUS = "TEMPERATURE_C";
		public static final String COLUMN_TEMPERATURE_FAHRENHEIT = "TEMPERATURE_F";
		public static final String COLUMN_PRESSURE_SEALEVEL_PSIG= "PRESSURE_SEALEVEL_PSIG";
		public static final String COLUMN_PRESSURE_ABOVESEALEVEL_PSIG= "PRESSURE_ABOVESEALEVEL_PSIG";
		public static final String COLUMN_PRESSURE_SEALEVEL_BARG= "PRESSURE_SEALEVEL_BARG";
		public static final String COLUMN_PRESSURE_ABOVESEALEVEL_BARG= "PRESSURE_ABOVESEALEVEL_BARG";
		
		//declare variables
		private SQLiteDatabase dbSqlite = null;
		private Context myContext;
		
		//set the context of a database
		public EmersonPtDBadapter(Context context) {
			super(context, DATABASE_NAME, null, 2);
			this.myContext = context;
		}
		
		//create database
		public void createDB() {
		    //boolean dbExists = DBExists();
		    
		    //checks if database exist, if not create and make a copy of database
		   // if (dbExists) {
		    //} else {
		    	dbSqlite = this.getReadableDatabase();
		    	dbSqlite.close();
		    	
		        try{
		        	copyDBFromResource();
		        	//Log.v("avian", "creating DB");
		        }catch (Exception e){
			        e.printStackTrace();
			    }		    
		    //}
		    
		    openDataBase();
		}
		
		//checks if database exist in the database path
		private boolean DBExists() {
		    SQLiteDatabase db = null;
		    try {
		        String databasePath = DATABASE_PATH + DATABASE_NAME;
		        db = SQLiteDatabase.openDatabase(databasePath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		    } catch (SQLiteException e) {
		        Log.e("PTCHART", "Database not found");
		    }
		    if (db != null) {
		        db.close();
		        Log.i(TAG, "Database Exists");
		    }
		    return db != null ? true : false;
		}
		
		//makes a copy of the database and place it on the database path
		private void copyDBFromResource() throws IOException{
		    InputStream inputStream = null;
		    OutputStream outStream = null;
		    String dbFilePath = DATABASE_PATH + DATABASE_NAME;
		    try {
		        inputStream = myContext.getAssets().open(DATABASE_NAME);
		        outStream = new FileOutputStream(dbFilePath);
		        byte[] buffer = new byte[1024];
		        int length;
		        while ((length = inputStream.read(buffer)) > 0) {
		            outStream.write(buffer, 0, length);
		        }
		        outStream.flush();
		        outStream.close();
		        inputStream.close();
		    } catch (IOException e) {
		        e.printStackTrace();
		        throw new Error("Problem copying database from resource file.");
		    }
		}
	    
		//open the database from the database path
		public EmersonPtDBadapter openDataBase() throws SQLException {
		    try{
		    	String myPath = DATABASE_PATH + DATABASE_NAME;
		 	    dbSqlite = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		    }catch (Exception e){
		    	e.printStackTrace();
		    }
			return this;
		}
		
		//close the database
		public synchronized void close() {
		    if (dbSqlite != null)
		        dbSqlite.close();
		    super.close();
		}
		
		public void onCreate(SQLiteDatabase db) {
			// do nothing
		}
		
		//makes upgrade if the database has new Version number
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	        Log.w("SqlHelper", "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
	        onCreate(db);
	    }
		
		public Cursor getCursorGivenExactPressure(float pressure, String tableName){
			
			String strPressure = String.format(Locale.ENGLISH, "%.02f", pressure);
			Cursor c = null;
			
			if (scPsig.isChecked()){
				if (isSeaLevel)
					c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_PRESSURE_SEALEVEL_PSIG + "= " + strPressure,  null, null, null, null, null);
				else
					c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_PRESSURE_ABOVESEALEVEL_PSIG + "= " + strPressure,  null, null, null, null, null);
			}
			else{
				if (isSeaLevel)
					c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_SEALEVEL_BARG },COLUMN_PRESSURE_SEALEVEL_BARG + "= " + strPressure,  null, null, null, null, null);
				else
					c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG },COLUMN_PRESSURE_ABOVESEALEVEL_BARG + "= " + strPressure,  null, null, null, null, null);				
			}
			
			if (c != null) {
	            c.moveToFirst();
	        }
			return c;
		}
		
		public Cursor getCursorForLowerRangePressure(float pressure, String tableName){
			Cursor c = null;
			
			if (scPsig.isChecked()){
				if (isSeaLevel)
					c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_PRESSURE_SEALEVEL_PSIG+"<"+pressure,  null, null, null, COLUMN_PRESSURE_SEALEVEL_PSIG+" DESC", "1");
				else
					c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_PRESSURE_ABOVESEALEVEL_PSIG+"<"+pressure,  null, null, null, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG+" DESC", "1");
			}
			else{
				if (isSeaLevel)
					c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_PRESSURE_SEALEVEL_BARG+"<"+pressure,  null, null, null, COLUMN_PRESSURE_SEALEVEL_BARG+" DESC", "1");
				else
					c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_PRESSURE_ABOVESEALEVEL_BARG+"<"+pressure,  null, null, null, COLUMN_PRESSURE_ABOVESEALEVEL_BARG+" DESC", "1");				
			}
			
			if (c != null) {
	            c.moveToFirst();
	        }
			return c;
		}
		
		public Cursor getCursorForHigherRangePressure(float pressure, String tableName){
			Cursor c = null;
			
			if (scPsig.isChecked()){
				if (isSeaLevel)
					c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_PRESSURE_SEALEVEL_PSIG+">"+pressure,  null, null, null, null, "1");
				else
					c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_PRESSURE_ABOVESEALEVEL_PSIG+">"+pressure,  null, null, null, null, "1");
			}
			else{
				if (isSeaLevel)
					c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_PRESSURE_SEALEVEL_BARG+">"+pressure,  null, null, null, null, "1");
				else
					c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_PRESSURE_ABOVESEALEVEL_BARG+">"+pressure,  null, null, null, null, "1");				
			}
			
			if (c != null) {
	            c.moveToFirst();
	        }
			return c;
		}
		
		public Cursor getCursorForLowerRangeTemperature(float temperature, String tableName){
			Cursor c = null;
			
			if (scFahrenheit.isChecked())
				c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_TEMPERATURE_FAHRENHEIT+"<"+temperature,  null, null, null, COLUMN_TEMPERATURE_FAHRENHEIT+" DESC", "1");
			else
				c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_TEMPERATURE_CELSIUS+"<"+temperature,  null, null, null, COLUMN_TEMPERATURE_CELSIUS+" DESC", "1");
			
			if (c != null) {
	            c.moveToFirst();
	        }
			return c;
		}
		
		public Cursor getCursorForHigherRangeTemperature(float temperature, String tableName){
			Cursor c = null;
			
			if (scFahrenheit.isChecked())
				c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_TEMPERATURE_FAHRENHEIT+">"+temperature,  null, null, null, null, "1");
			else
				c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_TEMPERATURE_CELSIUS+">"+temperature,  null, null, null, null, "1");
			
			if (c != null) {
	            c.moveToFirst();
	        }
			return c;
		}
		
		public Cursor getCursorGivenExactTemperature(float temperature, String tableName){
			String strTemperature = String.format(Locale.ENGLISH, "%.01f", temperature);
			Cursor c = null;

			if (scFahrenheit.isChecked())
				c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_TEMPERATURE_FAHRENHEIT+"="+strTemperature,  null, null, null, null, null);
			else
				c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG},COLUMN_TEMPERATURE_CELSIUS+"="+strTemperature,  null, null, null, null, null);
			
			if (c != null) {
	            c.moveToFirst();
	        }
			startManagingCursor(c);
			return c;
		}
		
		public Cursor getCursorGivenExactTemperatureInCelsius(float temperature, String tableName){
			String strTemperature = String.format(Locale.ENGLISH, "%.01f", temperature);
			Cursor c = null;

			c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG },COLUMN_TEMPERATURE_CELSIUS+"="+strTemperature,  null, null, null, null, null);
			
			if (c != null) {
	            c.moveToFirst();
	        }
			startManagingCursor(c);
			return c;
		}

		public Cursor getCursorGivenExactPressureInBarg(float pressureInBarg, String tableName){
			
			String strPressure = String.format(Locale.ENGLISH, "%.02f", pressureInBarg);
			
			Cursor c = null;
						
			if (isSeaLevel)
				c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_SEALEVEL_PSIG, COLUMN_PRESSURE_SEALEVEL_BARG },COLUMN_PRESSURE_SEALEVEL_BARG + "= " + strPressure,  null, null, null, null, null);
			else
				c = dbSqlite.query(tableName, new String[] { COLUMN_ID, COLUMN_TEMPERATURE_CELSIUS, COLUMN_TEMPERATURE_FAHRENHEIT, COLUMN_PRESSURE_ABOVESEALEVEL_PSIG, COLUMN_PRESSURE_ABOVESEALEVEL_BARG },COLUMN_PRESSURE_ABOVESEALEVEL_BARG + "= " + strPressure,  null, null, null, null, null);				
			
			if (c != null) {
				startManagingCursor(c);
	            c.moveToFirst();
			}
			return c;
		}

		public float getMaxTempF(String tableName)
		{
			float ret = Constants.MAX_TEMP_IN_FAHRENHEIT;
			Cursor c = null;
			c = dbSqlite.query(tableName, new String[] { "MAX(" + COLUMN_TEMPERATURE_FAHRENHEIT + ")"  },  null, null, null, null, null);
			if (c.moveToFirst())
				ret =c.getFloat(0); 
			return ret;
		}
		public float getMaxTempC(String tableName)
		{
			float ret = Constants.MAX_TEMP_IN_CELSIUS;
			Cursor c = null;
			c = dbSqlite.query(tableName, new String[] { "MAX(" + COLUMN_TEMPERATURE_CELSIUS + ")"  },  null, null, null, null, null);
			if (c.moveToFirst())
				ret =c.getFloat(0); 
			return ret;
		}
		public float getMinTempF(String tableName)
		{
			float ret = Constants.MIN_TEMP_IN_FAHRENHEIT;
			Cursor c = null;
			c = dbSqlite.query(tableName, new String[] { "MIN(" + COLUMN_TEMPERATURE_FAHRENHEIT + ")"  },  null, null, null, null, null);
			if (c.moveToFirst())
				ret =c.getFloat(0); 
			return ret;
		}
		public float getMinTempC(String tableName)
		{
			float ret = Constants.MIN_TEMP_IN_CELSIUS;
			Cursor c = null;
			c = dbSqlite.query(tableName, new String[] { "MIN(" + COLUMN_TEMPERATURE_CELSIUS + ")"  },  null, null, null, null, null);
			if (c.moveToFirst())
				ret =c.getFloat(0); 
			return ret;
		}
	}
	
	 private class MyCustomPagerAdapter extends PagerAdapter {
	        public int getCount() {
	            return refrigerants.length;
	        }
	        public Object instantiateItem(View collection, int position) {
	            LayoutInflater inflater = (LayoutInflater) collection.getContext()
	                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            View view = refrigerants[position].getView();
	            ((ViewPager) collection).addView(view, 0);
	            return view;
	        }
	        @Override
	        public void destroyItem(View arg0, int arg1, Object arg2) {
	            ((ViewPager) arg0).removeView((View) arg2);
	        }
	        @Override
	        public boolean isViewFromObject(View arg0, Object arg1) {
	            return arg0 == ((View) arg1);
	        }
	        @Override
	        public Parcelable saveState() {
	            return null;
	        }
	    }
	private int getRefrigerantIndex(String tableName)
	{
		int ret = 0;
		
		for (int i = 0; i < refrigerants.length; i++)
		{
			Refrigerant r = refrigerants[i];
			//Log.v("avian", r.getTableName());
			if (r.getTableName().equalsIgnoreCase(tableName) == true)
				return i;
		}
		return ret;
	}
	
	public void setTextViewValues()
	{
		if (etPressure.getText().toString().equals(""))
		{
			etPressure.setText("0.00");
		}
		
		if (etTemperature.getText().toString().equals(""))
		{
			etTemperature.setText("0.00");
		}
		
		if (etbulbtemperature.getText().toString().equals(""))
		{
			etbulbtemperature.setText("0.00");
		}
	}
	
	public float parseReplace(String numberToParse)
	{
		String formattedNumber;
		formattedNumber = numberToParse.replace(",", ".");
		return Float.parseFloat(formattedNumber);
	}
}