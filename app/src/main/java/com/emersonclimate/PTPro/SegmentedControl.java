/*
 * Created by Numlock Solutions, Co on 18 August 2011
 * Copyright 2011 Emerson Climate Technologies. All rights reserved.
 * 
 */

package com.emersonclimate.PTPro;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.RadioButton;

/*
 * This class is a segmented control that set the button view
 */

public class SegmentedControl extends RadioButton {
	private float mX;
	
    public SegmentedControl(Context context) {
        super(context);
    }

    public SegmentedControl(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SegmentedControl(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
    
    @Override
    public void onDraw(Canvas canvas) {
    	
        String text = this.getText().toString();
        Paint textPaint = new Paint();
        textPaint.setAntiAlias(true);
        float currentHeight = textPaint.measureText("x");

        textPaint.setTextSize(this.getTextSize());
        textPaint.setTextAlign(Paint.Align.CENTER);
        
        //set the button in blue background and text in white
        if (isChecked()) {
            GradientDrawable grad = new GradientDrawable(Orientation.TOP_BOTTOM, new int[] { 0xff000099, 0xff000099 });
            grad.setBounds(0, 0, this.getWidth(), this.getHeight()-((this.getHeight()/2)/2));
            grad.draw(canvas);
            grad.setCornerRadius(1.0f);
            textPaint.setColor(Color.WHITE);
        } else {
        	 //set the button in white background and text in gray
            GradientDrawable grad = new GradientDrawable(Orientation.TOP_BOTTOM, new int[] { 0xffffffff, 0xffcccccc });
            grad.setBounds(0, 0, this.getWidth(), this.getHeight()-((this.getHeight()/2)/2));
            grad.draw(canvas);
            grad.setCornerRadius(1.0f);
            textPaint.setColor(Color.GRAY);
        }

        float h = (this.getHeight() / 2) + (currentHeight/2)/2;
        canvas.drawText(text, mX, h, textPaint);
        
        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setStyle(Style.STROKE);
        Rect rect = new Rect(0, 0, this.getWidth(),this.getHeight()-((this.getHeight()/2)/2));
        canvas.drawRect(rect, paint);
        
    }

    @Override
    protected void onSizeChanged(int w, int h, int ow, int oh) {
        super.onSizeChanged(w, h, ow, oh);
        mX = w * 0.5f;
    }

}
