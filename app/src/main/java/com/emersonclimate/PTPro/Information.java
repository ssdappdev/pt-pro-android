/*
 * Created by Numlock Solutions, Co on 18 August 2011
 * Copyright 2011 Emerson Climate Technologies. All rights reserved.
 * 
 */

package com.emersonclimate.PTPro;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.util.Linkify;
import android.text.util.Linkify.TransformFilter;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class Information extends Activity {
	
	//declare variables
	private EditText etTell;
	private RelativeLayout btnCall, btnEmail, btnEULA;
	private TextView tvTextLink1, tvTextLink2, tvTextLink3, tvTextLink4;
	
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); 
        
        setContentView(R.layout.information);
        
        //initialize components
        tvTextLink1 = (TextView) findViewById(R.id.link1);
        tvTextLink2 = (TextView) findViewById(R.id.link2);
        tvTextLink3 = (TextView) findViewById(R.id.link3);
        tvTextLink4 = (TextView) findViewById(R.id.link4);
        etTell = (EditText) findViewById(R.id.entry);
        btnCall = (RelativeLayout) findViewById(R.id.rl_call);
        btnEmail = (RelativeLayout) findViewById(R.id.rl_mail);
        btnEULA = (RelativeLayout) findViewById(R.id.rl_eula);
        
        Pattern wikiWordMatcher = Pattern.compile("here");    // match pattern "here" in the sentence
        
        String locale = Locale.getDefault().toString();
		if(locale.equalsIgnoreCase("zh_cn")){
			wikiWordMatcher = Pattern.compile("这里");  
		}
                     

        String scheme = "";
        TransformFilter mentionFilter = new TransformFilter() {
            public final String transformUrl(final Matcher match, String url) {
                return new String("http://docs.google.com/gview?embedded=true&url=http://www.emersonclimate.com/Documents/FlowControls/pdf/2003ECT-75%20R9%20%20(1-10).pdf");
            }
        };
        Linkify.addLinks(tvTextLink1, wikiWordMatcher, scheme, null, mentionFilter);
        
        TransformFilter mentionFilter1 = new TransformFilter() {
            public final String transformUrl(final Matcher match, String url) {
                return new String("http://docs.google.com/gview?embedded=true&url=http://www.emersonclimate.com/Documents/FlowControls/pdf/2006ECT-27%20R4.pdf");
            }
        };

        Linkify.addLinks(tvTextLink2, wikiWordMatcher, scheme, null, mentionFilter1);
        
        TransformFilter mentionFilter2 = new TransformFilter() {
            public final String transformUrl(final Matcher match, String url) {
                return new String("http://docs.google.com/gview?embedded=true&url=http://www.emersonclimate.com/Documents/FlowControls/pdf/2007DS-19%20R5.pdf");
            }
        };

        Linkify.addLinks(tvTextLink3, wikiWordMatcher, scheme, null, mentionFilter2);
        
        TransformFilter mentionFilter3 = new TransformFilter() {
            public final String transformUrl(final Matcher match, String url) {
                return new String("http://estore.rrd.com/Emerson/");
            }
        };
        
        Linkify.addLinks(tvTextLink4, wikiWordMatcher, scheme, null, mentionFilter3);
        
        // on button CALL clicked, make a call
        btnCall.setOnClickListener(new Button.OnClickListener() {
        	public void onClick(View v) {
        		String toDial="tel:+00"+etTell.getText().toString();
        		startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(toDial)));
        	}
        });
        
        // On EMAIL clicked, send an email
        btnEmail.setOnClickListener(new OnClickListener (){
        	public void onClick(View v){
        		Intent sendEmail = new Intent(android.content.Intent.ACTION_SEND);
            	sendEmail.setType("plain/text");
            	String locale = Locale.getDefault().toString();
            	if(locale.equalsIgnoreCase("zh_cn"))
            	{
	            	sendEmail.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"info.China@Emerson.com"} );
            	}
            	else
            	{
            		sendEmail.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"Sales.Flowcontrols@emerson.com"} );
            	}
            	
            	sendEmail.putExtra(android.content.Intent.EXTRA_SUBJECT, "PT Pro Inquiry" );
            	sendEmail.putExtra(android.content.Intent.EXTRA_TEXT, "" );
            	startActivity(Intent.createChooser(sendEmail, "Send mail via"));
        	}
        });
        
        // On EULA clicked, display dialog
        btnEULA.setOnClickListener(new OnClickListener (){
        	public void onClick(View v){
        		showDialog();
        	}
        });
    }
    
    //show dialog
	private void showDialog() {
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle(R.string.alert_title);
		builder.setMessage(R.string.app_license_agreement);
		builder.setPositiveButton(getString(R.string.eula_done), new DialogInterface.OnClickListener() {
	           public void onClick(DialogInterface dialog, int id) {
	        	   dialog.cancel();
	           }
	    });
		AlertDialog alert = builder.create();
		alert.show();
	}
    
}