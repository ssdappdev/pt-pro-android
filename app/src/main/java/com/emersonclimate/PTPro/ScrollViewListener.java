/*
 * Created by Numlock Solutions, Co on 18 August 2011
 * Copyright 2011 Emerson Climate Technologies. All rights reserved.
 * 
 */

package com.emersonclimate.PTPro;

/*
 * This interface the horizontalscroll view
 */

public interface ScrollViewListener {

	void onScrollChanged(HorizontalScroll scrollView, int x, int y, int oldx, int oldy);
	
}
