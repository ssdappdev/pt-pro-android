package com.emersonclimate.PTPro;

import android.view.MotionEvent;

public interface TouchEventListener {
	void onTouchEvent(HorizontalScroll scrollView, MotionEvent ev);
}
